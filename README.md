# Jāgaro's Qubes Config

## Setup

In `dom0`:

```sh
mkdir ~/qubes-config
cd ~/qubes-config

url="https://gitlab.com/jagaro/qubes-config/-/archive/main/qubes-config-main.tar.gz"
qvm-run --pass-io --no-gui --dispvm debian-12-dvm \
  curl -s "$url" | tar xz --strip-components=1
```

## Security Check

**_Be sure to inspect the files before proceeding!_**

## Usage

Run all the config:

```sh
dom0/rc
```

Run configs from `10*` through `49*`:

```sh
dom0/rc [1-4]
```

Watch log:

```sh
tail -F dom0/log/latest.log
```
