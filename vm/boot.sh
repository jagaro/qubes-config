#!/bin/bash
#
# VM bootstrap
#
# shellcheck disable=SC2034

#
# Functions
#

qubes-config-require-not-dom0() {
  if [ "$(hostname)" = dom0 ] ||
    grep ^ID /etc/os-release | grep -iq qubes; then
    echo >&2 "$(basename "$0"): cannot run in dom0"
    return 2
  fi
}

qubes-config-require-root() {
  if [ "$(id -un)" != root ]; then
    echo >&2 "$(basename "$0"): must run as root"
    return 2
  fi
}

qubes-config-require-user() {
  local user
  user="$(qubes-config-vm-user)"
  if [ "$(id -un)" != "$user" ]; then
    echo >&2 "$(basename "$0"): must run as $user"
    return 2
  fi
}

qubes-config-vm-user() {
  qubesdb-read /default-user 2>/dev/null || echo user
}

qubes-config-vm-home() {
  (getent passwd "$(qubes-config-vm-user)" || echo :::::/home/user) |
    cut -d : -f 6
}

#
# Environment
#

QUBES_CONFIG_DIR="$(realpath -m "${BASH_SOURCE[0]}/../..")"
QUBES_CONFIG_VM_DIR="$QUBES_CONFIG_DIR/vm"
QUBES_CONFIG_VM_BIN_DIR="$QUBES_CONFIG_VM_DIR/bin"
QUBES_CONFIG_VM_LIB_DIR="$QUBES_CONFIG_VM_DIR/lib"
QUBES_CONFIG_VM_QUBE_DIR="$QUBES_CONFIG_VM_DIR/qube"
QUBES_CONFIG_VM_SHARE_DIR="$QUBES_CONFIG_VM_DIR/share"

if ! echo ":$PATH:" | grep -q ":$QUBES_CONFIG_VM_BIN_DIR:"; then
  export PATH="$QUBES_CONFIG_VM_BIN_DIR:$PATH"
fi

qubes-config-require-not-dom0
