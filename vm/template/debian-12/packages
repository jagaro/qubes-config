#!/bin/bash

set -euo pipefail

. "$(realpath -m "${BASH_SOURCE[0]}/../../../boot.sh")"
qubes-config-require-root

# 2023-10-25: don't use u2f/ctap anymore as it seems to break modern FIDO2/Passkeys

PACKAGES=(
  # qubes
    qubes-gpg-split
  # needed for time sync
    systemd-timesyncd
  # tools
    bind9-dnsutils bind9-host iotop iptraf-ng jq netcat-openbsd
    nmap rsync tcpdump
  # encryption
    age gnupg minisign pcscd scdaemon
  # python
    python3 python3-dev python3-pip python3-venv
  # pyenv
    make
    build-essential
    libssl-dev
    zlib1g-dev
    libbz2-dev
    libreadline-dev
    libsqlite3-dev
    wget
    curl
    llvm
    libncurses5-dev
    libncursesw5-dev
    xz-utils
    tk-dev
    libffi-dev
    liblzma-dev
    python3-openssl
    git
  # yubikey manager
    libpcsclite-dev pcscd scdaemon swig
  # web development
    hugo s3cmd
  # go
    golang-go
  # android
    adb fastboot
  # flatpak
    flatpak
  # applications
    chromium ffmpeg gimp libreoffice vlc
)

cat <<EOF >/etc/apt/sources.list
deb https://deb.debian.org/debian bookworm main contrib non-free-firmware
deb https://deb.debian.org/debian bookworm-updates main contrib non-free-firmware
deb https://deb.debian.org/debian-security bookworm-security main contrib non-free-firmware
EOF

apt-get update

DEBIAN_FRONTEND=noninteractive apt-get dist-upgrade -y

apt-get autoremove -y

DEBIAN_FRONTEND=noninteractive apt-get install -yqq "${PACKAGES[@]}"

apt-get clean

/usr/lib/qubes/upgrades-status-notify
