#!/bin/bash

set -euo pipefail

. "$(realpath -m "${BASH_SOURCE[0]}/../../../boot.sh")"
qubes-config-require-root

# 2023-10-25: don't use u2f/ctap anymore as it seems to break modern FIDO2/Passkeys

PACKAGES=(
  # painful without these
  man-db manpages qubes-core-agent-passwordless-root xfce4-terminal
  # core network tools
  bind9-host curl iotop iptraf-ng jq netcat-openbsd nmap rsync tcpdump
  # encryption
  age gnupg minisign pcscd scdaemon
  # sys-net
  firmware-iwlwifi qubes-core-agent-network-manager systemd-timesyncd
  # sys-firewall (qubes-updates-proxy)
  qubes-core-agent-dom0-updates
  # sys-usb
  qubes-usb-proxy
  # sys-ipv6
  libnss-myhostname linux-image-amd64 wireguard
  # vault
  gnupg keepassxc openssh-client qubes-gpg-split scdaemon
  # flatpak qubes
  flatpak
)

DOWNLOAD_PACKAGES=(
  # sys-ipv6
  systemd-resolved
)

apt-get update

DEBIAN_FRONTEND=noninteractive apt-get dist-upgrade -y

apt-get autoremove -y

DEBIAN_FRONTEND=noninteractive apt-get install -yqq \
  "${PACKAGES[@]}"

apt-get clean

DEBIAN_FRONTEND=noninteractive apt-get install --download-only -yqq \
  "${DOWNLOAD_PACKAGES[@]}"

/usr/lib/qubes/upgrades-status-notify
