#!/bin/bash

set -euo pipefail

. "$(realpath -m "${BASH_SOURCE[0]}/../../boot.sh")"
qubes-config-require-user

if [ "${1:-}" = beta ]; then
  APP_NAME="Firefox Beta"
  APP_EXT=-beta
elif [ "${1:-}" = nightly ]; then
  APP_NAME="Firefox Nightly"
  APP_EXT=-nightly
else
  APP_NAME="Firefox"
  APP_EXT=
fi

INSTALL_DESKTOP_PATH="$HOME/.local/share/applications/org.mozilla.firefox$APP_EXT.desktop"
INSTALL_EXEC_PATH="$HOME/.local/bin/firefox$APP_EXT"
APP_DIR="$HOME/.local/firefox$APP_EXT"
APP_DIST_DIR="$APP_DIR/dist"
APP_EXEC_PATH="$APP_DIST_DIR/firefox"
APP_DESKTOP_PATH="$APP_DIR/org.mozilla.firefox$APP_EXT.desktop"
APP_ICON_PATH="$APP_DIST_DIR/browser/chrome/icons/default/default128.png"
APP_ARCHIVE_URL="https://download.mozilla.org/?product=firefox$APP_EXT-latest-ssl&os=linux64&lang=en-US"
APP_ARCHIVE_PATH="$APP_DIR/firefox.tar.bz2"

mkdir -p \
  "$(dirname "$INSTALL_EXEC_PATH")" \
  "$(dirname "$INSTALL_DESKTOP_PATH")" \
  "$APP_DIR"

download-latest "$APP_ARCHIVE_URL" "$APP_ARCHIVE_PATH"

if [ -f "$APP_ARCHIVE_PATH.new" ] || [ ! -d "$APP_DIST_DIR" ]; then
  rm -rf "$APP_DIST_DIR"
  mkdir -p "$APP_DIST_DIR"
  tar -xa -C "$APP_DIST_DIR" --strip-components=1 -f "$APP_ARCHIVE_PATH"
fi

cat >"$APP_DESKTOP_PATH" <<EOF
[Desktop Entry]
Version=1.0
Name=$APP_NAME
GenericName=Web Browser
Comment=Browse the Web
Exec=$APP_EXEC_PATH %u
Icon=$APP_ICON_PATH
Terminal=false
Type=Application
MimeType=text/html;text/xml;application/xhtml+xml;application/vnd.mozilla.xul+xml;text/mml;x-scheme-handler/http;x-scheme-handler/https;
StartupNotify=true
Categories=Network;WebBrowser;
Keywords=web;browser;internet;
Actions=new-window;new-private-window;

[Desktop Action new-window]
Name=Open a New Window
Exec=$APP_EXEC_PATH %u

[Desktop Action new-private-window]
Name=Open a New Private Window
Exec=$APP_EXEC_PATH --private-window %u
EOF

ln -srf "$APP_EXEC_PATH" "$INSTALL_EXEC_PATH"
ln -srf "$APP_DESKTOP_PATH" "$INSTALL_DESKTOP_PATH"
