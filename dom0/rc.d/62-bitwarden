#!/bin/bash

set -euo pipefail

. "$(realpath -m "${BASH_SOURCE[0]}/../../boot.sh")"
qubes-config-dom0-setup-logging

VMNAME=bitwarden

qvm-create-if-not-exists $VMNAME

qvm-prefs $VMNAME autostart --default
qvm-prefs $VMNAME include_in_backups False
qvm-prefs $VMNAME label green
qvm-prefs $VMNAME maxmem --default
qvm-prefs $VMNAME memory --default
qvm-prefs $VMNAME netvm --default

qvm-features-set-menu-items $VMNAME \
  chromium.desktop \
  com.bitwarden.desktop.desktop \
  firefox-esr.desktop \
  org.gnome.Terminal.desktop

qvm-service --enable $VMNAME gui-agent-clipboard-wipe
qvm-service --enable $VMNAME qubes-u2f-proxy

qvm-volume-set-size $VMNAME private 5

qvm-firewall-reset $VMNAME

qvm-run-config --root $VMNAME share/tools
qvm-run-config --user $VMNAME share/bitwarden-desktop
qvm-run-config --user $VMNAME share/empty-keyring
qvm-run-config --root $VMNAME share/simple-firefox-esr https://vault.bitwarden.com/

qvm-firewall-add-https $VMNAME api.bitwarden.com
qvm-firewall-add-https $VMNAME api.pwnedpasswords.com
qvm-firewall-add-https $VMNAME icons.bitwarden.com
qvm-firewall-add-https $VMNAME icons.bitwarden.net
qvm-firewall-add-https $VMNAME identity.bitwarden.com
qvm-firewall-add-https $VMNAME vault.bitwarden.com
qvm-firewall-add-https $VMNAME bitwardenxx5keu3w.blob.core.windows.net
qvm-firewall-add-drop $VMNAME

qvm-sync-appmenus-quietly $VMNAME
