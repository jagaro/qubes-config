#!/bin/bash

set -euo pipefail

. "$(realpath -m "${BASH_SOURCE[0]}/../../boot.sh")"

qubes-config-check-args 3 "VMNAME VOLUME SIZE_GB" "$@"
VMNAME="$1"
VOLUME="$2"
NEW_SIZE_GB="$3"
qubes-config-dom0-check-vm "$VMNAME"

OLD_SIZE="$(qvm-volume info "$VMNAME:$VOLUME" size)"
NEW_SIZE="$((NEW_SIZE_GB * 2 ** 30))"

if [ "$OLD_SIZE" != "$NEW_SIZE" ]; then
  qubes-config-dom0-log "$VMNAME" "resizing $VOLUME to ${NEW_SIZE_GB}GB"
  qvm-volume resize "$VMNAME:$VOLUME" "$NEW_SIZE"
fi
