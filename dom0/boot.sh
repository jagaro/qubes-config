#!/bin/bash
#
# dom0 bootstrap
#
# shellcheck disable=SC2034

#
# Functions
#

qubes-config-require-dom0() {
  if [ "$(hostname)" != dom0 ] ||
    grep ^ID /etc/os-release | grep -viq qubes; then
    echo >&2 "$(basename "$0"): must run in dom0"
    return 2
  fi
}

qubes-config-check-args() {
  local min="$1"
  local usage="$2"
  shift 2
  if [ $# -lt "$min" ] || [ "${1:-}" = -h ] || [ "${1:-}" = --help ]; then
    echo >&2 "usage: $(basename "$0") $usage"
    return 2
  fi
}

qubes-config-dom0-check-vm() {
  local vmname="$1"
  if ! qvm-check --quiet -- "$vmname" 2>/dev/null; then
    echo >&2 "$(basename "$0"): no such VM ${vmname@Q}"
    return 2
  fi
}

qubes-config-dom0-setup-logging() {
  [ -n "$QUBES_CONFIG_DOM0_LOG_PATH" ] && return
  QUBES_CONFIG_DOM0_LOG_PATH="$QUBES_CONFIG_DOM0_LOG_DIR/$(date +%Y%m%d).log"
  touch "$QUBES_CONFIG_DOM0_LOG_PATH"
  (
    latest_log_path="$QUBES_CONFIG_DOM0_LOG_DIR/latest.log"
    ln -srf "$QUBES_CONFIG_DOM0_LOG_PATH" "$latest_log_path"
  )
}

#
# Environment
#

QUBES_CONFIG_DIR="$(realpath -m "${BASH_SOURCE[0]}/../..")"
QUBES_CONFIG_DOM0_BIN_DIR="$QUBES_CONFIG_DIR/dom0/bin"
QUBES_CONFIG_DOM0_LOG_DIR="$QUBES_CONFIG_DIR/dom0/log"
QUBES_CONFIG_DOM0_LOG_PATH="${QUBES_CONFIG_DOM0_LOG_PATH:-}"
QUBES_CONFIG_DOM0_RC_DIR="$QUBES_CONFIG_DIR/dom0/rc.d"
QUBES_CONFIG_HASHER_PATH="$QUBES_CONFIG_DIR/hash"

export QUBES_CONFIG_DOM0_LOG_PATH CONFIG_CONFIG_HASH

if ! echo ":$PATH:" | grep -q ":$QUBES_CONFIG_DOM0_BIN_DIR:"; then
  export PATH="$QUBES_CONFIG_DOM0_BIN_DIR:$PATH"
fi

# for running VM
VM_QUBES_CONFIG_DIR="/rw/qubes-config"
VM_QUBES_CONFIG_HASHER_PATH="$VM_QUBES_CONFIG_DIR/hash"

qubes-config-require-dom0
